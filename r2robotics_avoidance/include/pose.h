#ifndef POSE
#define POSE

#include "angles.h"
#include <r2robotics_msgs/RobotPose.h>

class Pose {
 public:
  Pose() {};
  Pose(double _x, double _y)
  {
    x = _x;
    y = _y;
    yaw = 0;
  }
  Pose(double _x, double _y, double _yaw)
  {
    x = _x;
    y = _y;
    yaw = DEG_TO_180(_yaw);
  }
  Pose( r2robotics_msgs::RobotPose p )
  {
    x = p.x;
    y = p.y;
    yaw = p.yaw;
  }
  void update(Pose pos)
  {
    x = pos.x;
    y = pos.y;
    yaw = DEG_TO_180(pos.yaw);
  }
  void update(double _x, double _y, double _yaw)
  {
    x = _x;
    y = _y;
    yaw = DEG_TO_180(_yaw);
  }

  double x;
  double y;
  double yaw;

  bool operator == (const Pose &s2) const
  {
    return ( (x == s2.x) && (y == s2.y) && (yaw == s2.yaw) );
  }

  bool operator != (const Pose &s2) const
  {
    return ( (x != s2.x) || (y != s2.y) || (yaw != s2.yaw) );
  }

  Pose operator * (const double &k) const
  {
    return Pose( x*k, y*k, yaw*k );
  }

  Pose operator * (const Pose &s2) const
  {
    return Pose( x*s2.x, y*s2.y, yaw*s2.yaw );
  }

  Pose operator + (const Pose &s2) const
  {
    return Pose( x+s2.x, y+s2.y, yaw+s2.yaw );
  }

  Pose operator - (const Pose &s2) const
  {
    return Pose( x-s2.x, y-s2.y, yaw-s2.yaw );
  }

  Pose operator / (const Pose &s2) const
  {
    return Pose( x/s2.x, y/s2.y, yaw/s2.yaw );
  }

  Pose operator / (const double &k) const
  {
    return Pose( x/k, y/k, yaw/k );
  }
  friend std::ostream& operator<< (std::ostream& stream, const Pose& pose)
  {
    stream << std::fixed << "(" << pose.x << ", " << pose.y << ", " << pose.yaw << ")";
    return stream;
  }
};

#endif // POSE
